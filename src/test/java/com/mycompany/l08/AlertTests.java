package com.mycompany.l08;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.File;

import static org.openqa.selenium.support.ui.ExpectedConditions.alertIsPresent;
import static org.testng.Assert.assertEquals;

public class AlertTests extends TestBase {

    @BeforeMethod
    public void before() {
        var url = "file:///" + new File(STATIC_RESOURCES + "alerts.html").getAbsolutePath();
        driver.get(url);
    }

    @Test
    public void checkAlertCapability() throws InterruptedException {
        WebElement button = driver.findElement(By.id("alert"));
        button.click();
        Thread.sleep(3_000);

        WebElement inputField = driver.findElement(By.id("field"));
        inputField.sendKeys("selenium text");
        Thread.sleep(3_000);
    }

    @Test
    public void acceptDismissConfirm() {
        WebElement button = driver.findElement(By.id("confirm"));
        button.click();

        WebElement inputField = driver.findElement(By.id("field"));
        String value = inputField.getAttribute("value");
        System.out.println("Alert is accepted: " + value);
    }

    @Test
    public void acceptDismissConfirm2() throws InterruptedException {
        WebElement button = driver.findElement(By.id("confirm"));
        button.click();

        Alert alert = driver.switchTo().alert();
        Thread.sleep(3_000);
        alert.dismiss();

        //browser capability will work below
        WebElement inputField = driver.findElement(By.id("field"));
        String value = inputField.getAttribute("value");
        System.out.println("Alert is accepted? : " + value);
    }

    @Test
    public void confirmWithLatency() {
        WebElement button = driver.findElement(By.id("confirmWithLatency"));
        button.click();

      //  Alert alert = driver.switchTo().alert();
        Wait<WebDriver> wait = new WebDriverWait(driver, 5_000);
        Alert alert = wait.until(alertIsPresent());

        alert.accept();
    }

    @Test
    public void enterText() throws InterruptedException {
        WebElement button = driver.findElement(By.id("prompt"));
        button.click();

        Wait<WebDriver> wait = new WebDriverWait(driver, 5_000);
        Alert alert = wait.until(alertIsPresent());
        var text = alert.getText();
        System.out.println("Text in prompt: " + text);
        alert.sendKeys(text);

        Thread.sleep(10_000);

        WebElement inputField = driver.findElement(By.id("field"));
        String value = inputField.getAttribute("value");
        assertEquals(value, text, "Text is incorrect");
    }
}
