package com.mycompany.l08;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.File;
import java.util.List;
import java.util.Set;
import java.util.function.Supplier;

public class WindowsTests extends TestBase {


    @BeforeMethod
    public void before() {
        var url = "file:///" + new File(STATIC_RESOURCES + "switchWindows.html").getAbsolutePath();
        driver.get(url);
    }

    @Test
    public void switchWindows() {

        //get id of the current window
        String originalWindow = driver.getWindowHandle();
        System.out.println("originalWindow: " + originalWindow);

        //get ids of all open windows
        Set<String> existingWindows = driver.getWindowHandles();

        //click a button 'New window'
        driver.findElement(By.id("newWindow")).click();
        //((JavascriptExecutor) driver).executeScript("window.open()");

        //wait a new window appeared
        Wait<WebDriver> wait = new WebDriverWait(driver, 5_000);
        var newWindow = wait.until(anyWindowOtherThan(existingWindows));

        //driver does not switch to another window automatically
        System.out.println("window after click 'New window': " + driver.getWindowHandle());

        //switch to a new window
        driver.switchTo().window(newWindow);
        System.out.println("new window: " + driver.getWindowHandle());

        //close the new window
        driver.close();

        //driver does not switch to another window automatically after close
        try {
            driver.getWindowHandle();
        } catch (NoSuchWindowException ex) {
            System.out.println(ex.getClass().getName() + " is caught");
        }

        //go back to the original window
        driver.switchTo().window(originalWindow);
        System.out.println("switch to the original window: " + driver.getWindowHandle());

        driver.quit();
    }

    @Test
    public void windowOrTab() {
        List.of((Supplier<WebDriver>) FirefoxDriver::new, ChromeDriver::new)
                .forEach(supplier -> {
                    driver = supplier.get();
                    driver.get("https://google.ru");
                    ((JavascriptExecutor) driver).executeScript("window.open()");
                });
    }

    private static ExpectedCondition<String> anyWindowOtherThan(Set<String> oldWindows) {
        return webDriver -> {
            if (webDriver != null) {
                Set<String> handles = webDriver.getWindowHandles();
                handles.removeAll(oldWindows);
                return handles.size() > 0 ? handles.iterator().next() : null;
            }
            return null;
        };
    }

}
